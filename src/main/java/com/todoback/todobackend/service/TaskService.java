package com.todoback.todobackend.service;

import com.sun.source.util.TaskListener;
import com.todoback.todobackend.domain.Task;
import com.todoback.todobackend.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {

    @Autowired
    TaskRepository taskRepository;

    public List<Task> findAllByUserId(int currentUserId) {
        return taskRepository.findAllByUserId(currentUserId);
    }

    public Task findTaskById(int id){
        Optional<Task> greetingOptional = taskRepository.findById(id);
        if (greetingOptional.isPresent()) {
            return greetingOptional.get();
        } else {
            Task taskEmpty = new Task();
            taskEmpty.setId(0);
            taskEmpty.setName("Nie znaleziono");
            return taskEmpty;
        }
    }

    public Task createNewTask(Task newTask){
        if (taskRepository.findAll().isEmpty()) {
            newTask.setId(1);
            taskRepository.save(newTask);
        } else if (newTask.getId() == 0) {
            List<Task> tasks = taskRepository.findAll();
            Task maxTask = tasks.get(0);
            for (int i = 1; i < tasks.size(); i++) {
                if (tasks.get(i).getId() > maxTask.getId()) {
                    maxTask = tasks.get(i);
                }
            }
            int maxId = maxTask.getId();
            newTask.setId(maxId + 1);
            return taskRepository.save(newTask);
        } else {
            Optional<Task> greetingOptional = taskRepository.findById(newTask.getId());
            if (greetingOptional.isPresent()) {
                newTask.setName("Nie mozna zapisac obiektu o tym id");
                return newTask;
            } else {
                taskRepository.save(newTask);
            }
        }

        return taskRepository.save(newTask);
    }

    public void markTaskAsCompleted(Task task1){
        Optional<Task> optionalTask = taskRepository.findById(task1.getId());
        if (optionalTask.isPresent()) {
            Task foundTask = optionalTask.get();
            foundTask.setCompleted(true);
            taskRepository.save(foundTask);
        }
    }

    public void deleteTask(Task task){
        Optional<Task> optionalTask = taskRepository.findById(task.getId());
        if (optionalTask.isPresent()) {
            Task foundTask = optionalTask.get();
            taskRepository.delete(foundTask);
        }
    }

    public void deleteAllTasks(List<Task> tasks){
        for (Task task : tasks) {
            Optional<Task> optionalTask = taskRepository.findById(task.getId());
            if (optionalTask.isPresent()) {
                Task foundTask = optionalTask.get();
                taskRepository.delete(foundTask);
            }
        }
    }

    public void editTask(Task task){
        Optional<Task> optionalTask = taskRepository.findById(task.getId());
        if (optionalTask.isPresent()) {
            Task foundTask = optionalTask.get();
            foundTask.setName(task.getName());
            foundTask.setPriority(task.getPriority());
            foundTask.setDescription(task.getDescription());
            taskRepository.save(foundTask);
        }
    }

    public List<Task> prepareOpenTasks(){
        return taskRepository.findAllByCompletedFalse();
    }


}
