package com.todoback.todobackend.repository;

import com.todoback.todobackend.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {

    List<Task> findAllByUserId(int userId);
    List<Task> findAllByCompletedFalse();

}
