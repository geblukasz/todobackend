package com.todoback.todobackend.controller;

import com.todoback.todobackend.domain.AuthenticationDTO;
import com.todoback.todobackend.domain.RegisterDTO;
import com.todoback.todobackend.domain.Task;
import com.todoback.todobackend.domain.User;
import com.todoback.todobackend.repository.TaskRepository;
import com.todoback.todobackend.service.TaskService;
import com.todoback.todobackend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.Optional;

@RestController
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    UserService userService;

    @GetMapping("/task/list/{currentUserId}")
    public List<Task> tasks(@PathVariable int currentUserId) {
        return taskService.findAllByUserId(currentUserId);
    }

    @GetMapping("/task/{id}")
    public Task task(@PathVariable int id) {
        return taskService.findTaskById(id);
    }

    @PostMapping("/task")
    public Task createNewTask(@RequestBody Task newTask) {
        return taskService.createNewTask(newTask);
    }

    @PostMapping("/task/modification")
    public void markTaskAsCompleted(@RequestBody Task task1) {
        taskService.markTaskAsCompleted(task1);
    }

    @PostMapping("/task/delete")
    public void deleteTask(@RequestBody Task task) {
        taskService.deleteTask(task);
    }

    @PostMapping("/task/deleteAll")
    public void deleteAllTasks(@RequestBody List<Task> tasks) {
        taskService.deleteAllTasks(tasks);
    }


    @PostMapping("/task/edit")
    public void editTask(@RequestBody Task task) {
        taskService.editTask(task);
    }

    @PostMapping("/user/authenticate")
    public AuthenticationDTO authenticateUser(@RequestBody User user) {
        return userService.authenticateUser(user);
    }

    @PostMapping("/user/register")
    public void registerUser(@RequestBody RegisterDTO registerDTO) {
        userService.registerUser(registerDTO);
    }

    @GetMapping("/user/activation/{activationId}")
    public RedirectView activateUser(@PathVariable String activationId) {
        userService.activateUser(activationId);
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("http://localhost:4200/activated");
        return redirectView;
    }

    @GetMapping("/user/id/{username}")
    public int prepareUserId(@PathVariable String username) {
        return userService.prepareUserIdFromUsername(username);
    }

}
